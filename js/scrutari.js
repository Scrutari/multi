/*** r1413 ***/
var Scrutari = {};
Scrutari.log = function (msg) {
    if ((console) && (console.log)) {
        console.log(msg);
    }
};
Scrutari.logError = function (error) {
    var errorMessage = "Scrutari Request Error [key = " + error.key + " | parameter = " + error.parameter;
    if (error.hasOwnProperty("value")) {
        errorMessage += " | value = " + error.value;
    }
    if (error.hasOwnProperty("array")) {
        errorMessage += " | array = (";
        for(var i = 0; i < error.array.length; i++) {
            if (i > 0) {
                errorMessage += ";";
            }
            var obj = error.array[i];
            errorMessage += obj.key;
            if (obj.hasOwnProperty("value")) {
                errorMessage += "=" + obj.value;
            }
        }
        errorMessage += ")";
    }
    errorMessage += "}";
    Scrutari.log(errorMessage);
};
Scrutari.convert = function (jqArgument) {
    if (jqArgument.jquery) {
        return jqArgument;
    } else {
        return $(jqArgument);
    }
};
Scrutari.exists = function (jqArgument) {
    return Scrutari.convert(jqArgument).length > 0;
};
 Scrutari.escape = function (text) {
     var result = "";
    for(var i = 0; i < text.length; i++) {
        carac = text.charAt(i);
        switch (carac) {
            case '&':
                result += "&amp;";
                break;
            case '"':
                result += "&quot;";
                break;
            case '<':
                result += "&lt;";
                break;
            case '>':
                result += "&gt;";
                break;
            case '\'':
                result += "&#x27;";
                break;
            default:
                result += carac;
        }
    }
    return result;
 };
Scrutari.Loc = function (map) {
    if (map) {
        this.map = map;
    } else {
        this.map = new Object();
    }
};
Scrutari.Loc.prototype.putAll = function (map) {
    for(var key in map) {
        this.map[key] = map[key];
    }
};
Scrutari.Loc.prototype.putLoc = function (locKey, locText) {
    this.map[locKey] = locText;
};
Scrutari.Loc.prototype.loc = function (locKey) {
    if (!this.map.hasOwnProperty(locKey)) {
        return locKey;
    }
    var text = this.map[locKey];
    var argLength = arguments.length;
    if (argLength > 1) {
        for(var i = 0; i < argLength; i++) {
            var p = i -1;
            var mark = "{" + p + "}";
            text = text.replace(mark, arguments[i]);
        }
    }
    return text;
};
Scrutari.Loc.prototype.escape = function (locKey) {
    return Scrutari.escape(this.loc.apply(this, arguments));
};
Scrutari.Config = function (name, engineUrl, langUi, origin) {
    this.name = name;
    this.engineUrl = engineUrl;
    this.langUi = langUi;
    this.origin = origin;
    this.ficheFields = "";
    this.plageLength = 50;
    this.groupSortFunction = Scrutari.Utils.sortGroupByFicheCount;
};
Scrutari.Config.prototype.getName = function () {
    return this.name;
};
Scrutari.Config.prototype.getJsonUrl = function () {
    return this.engineUrl + "json";
};
Scrutari.Config.prototype.getLangUi = function () {
    return this.langUi;
};
Scrutari.Config.prototype.getOrigin = function () {
    return this.origin;
};
Scrutari.Config.prototype.getPlageLength = function () {
    return this.plageLength;
};
Scrutari.Config.prototype.setPlageLength = function (plageLength) {
    this.plageLength = plageLength;
};
Scrutari.Config.prototype.getOdsUrl = function (qId) {
    return this.engineUrl + "export/" +  "result_" + qId + "_" + this.langUi + ".ods";
};
Scrutari.Config.prototype.getAtomUrl = function (qId) {
    var date = new Date();
    var dateString = date.getFullYear() + "-";
    var mois = date.getMonth() + 1;
    if (mois < 10) {
        dateString += "0";
    }
    dateString += mois;
    dateString += "-";
    var jour = date.getDate();
    if (jour < 10) {
        dateString += "0";
    }
    dateString += jour;
    return this.engineUrl + "feed/" + "fiches_" + this.langUi + ".atom?qid=" + qId + "&all=" + dateString;
};
Scrutari.Config.prototype.getFicheFields = function () {
    return this.ficheFields;
};
Scrutari.Config.prototype.setFicheFields = function (ficheFields) {
    this.ficheFields = ficheFields;
};
Scrutari.Config.prototype.setGroupSortFunction = function (groupSortFunction) {
    this.groupSortFunction = groupSortFunction;
};
Scrutari.Config.prototype.getGroupSortFunction = function () {
    return this.groupSortFunction;
};
Scrutari.Ajax = {};
Scrutari.Ajax.loadBaseArray = function (baseArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "base";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "baseArray", baseArrayCallback);
        }
    });
};
Scrutari.Ajax.loadCorpusArray = function (corpusArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "corpus";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "corpusArray", corpusArrayCallback);
        }
    });
};
Scrutari.Ajax.loadThesaurusArray = function (thesaurusArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "thesaurus";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "thesaurusArray", thesaurusArrayCallback);
        }
    });
};
Scrutari.Ajax.loadMotcleArray = function (motcleArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "motcle";
    Scrutari.Ajax.check(requestParameters, "motclefields", "motcleid,libelles");
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "motcleArray", motcleArrayCallback);
        }
    });
};
Scrutari.Ajax.loadFicheArray = function (ficheArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "fiche";
    Scrutari.Ajax.check(requestParameters, "fichefields", "titre");
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "ficheArray", ficheArrayCallback);
        }
    });
};
Scrutari.Ajax.loadCategoryArray = function (categoryArrayCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "category";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "categoryArray", categoryArrayCallback);
        }
    });
};
Scrutari.Ajax.loadEngineInfo = function (engineInfoCallback, scrutariConfig, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "engine";
    Scrutari.Ajax.check(requestParameters, "data", "all");
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 1);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "engineInfo", engineInfoCallback);
        }
    });
};
Scrutari.Ajax.loadFicheSearchResult = function (ficheSearchResultCallback, scrutariConfig, requestParameters, scrutariErrorCallback) {
    var defaultFicheFields = scrutariConfig.getFicheFields();
    if (!defaultFicheFields) {
        defaultFicheFields = "codecorpus,mtitre,msoustitre,mattrs_all,mcomplements,annee,href,icon";
    }
    requestParameters.type = "q-fiche";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "fichefields", defaultFicheFields);
    Scrutari.Ajax.check(requestParameters, "motclefields", "mlibelles");
    Scrutari.Ajax.check(requestParameters, "q-mode", "intersection");
    Scrutari.Ajax.check(requestParameters, "origin", scrutariConfig.getOrigin());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    Scrutari.Ajax.check(requestParameters, "start", 1);
    Scrutari.Ajax.check(requestParameters, "limit", scrutariConfig.getPlageLength() * 2);
    Scrutari.Ajax.check(requestParameters, "starttype", "in_all");
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "ficheSearchResult", ficheSearchResultCallback, scrutariErrorCallback);
        }
    });
};
Scrutari.Ajax.loadExistingFicheSearchResult = function (existingFicheSearchResultCallback, scrutariConfig, requestParameters) {
    var defaultFicheFields = scrutariConfig.getFicheFields();
    if (!defaultFicheFields) {
        defaultFicheFields = "codecorpus,mtitre,msoustitre,mattrs_all,mcomplements,annee,href,icon";
    }
    requestParameters.type = "q-fiche";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "fichefields", defaultFicheFields);
    Scrutari.Ajax.check(requestParameters, "motclefields", "");
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 2);
    Scrutari.Ajax.check(requestParameters, "start", 1);
    Scrutari.Ajax.check(requestParameters, "limit", scrutariConfig.getPlageLength() * 2);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "ficheSearchResult", existingFicheSearchResultCallback);
        }
    });
};
Scrutari.Ajax.loadGeoJson = function (geojsonCallback, scrutariConfig, requestParameters, scrutariErrorCallback) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "geojson";
    Scrutari.Ajax.check(requestParameters, "langui", scrutariConfig.getLangUi());
    Scrutari.Ajax.check(requestParameters, "fichefields", "titre");
    Scrutari.Ajax.check(requestParameters, "motclefields", "");
    Scrutari.Ajax.check(requestParameters, "origin", scrutariConfig.getOrigin());
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 1);
    $.ajax({
        url: scrutariConfig.getJsonUrl(),
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            if (data.hasOwnProperty("error")) {
                if (scrutariErrorCallback) {
                    scrutariErrorCallback(data.error);
                } else {
                    Scrutari.logError(data.error);
                }
            } else {
                Scrutari.Ajax.logWarnings(data);
                geojsonCallback(data);
            }
        }
    });
};
Scrutari.Ajax.success = function(ajaxResult, objectName, objectCallback, scrutariErrorCallback) {
    if (ajaxResult.hasOwnProperty("error")) {
        if (scrutariErrorCallback) {
            scrutariErrorCallback(ajaxResult.error);
        } else {
            Scrutari.logError(ajaxResult.error);
        }
    } else {
        Scrutari.Ajax.logWarnings(ajaxResult);
        if (!ajaxResult.hasOwnProperty(objectName)) {
            $.error(objectName + " object is missing in json response");
        } else {
            objectCallback(ajaxResult[objectName]);
        }
    }
};
Scrutari.Ajax.logWarnings = function (ajaxResult) {
    if (ajaxResult.hasOwnProperty("warnings")) {
        warningsMessage = "Scrutari Request Warnings [";
        for(var i = 0; i < ajaxResult.warnings.length; i++) {
            if (i > 0) {
                warningsMessage += ";";
            }
            var warning = ajaxResult.warnings[i];
            warningsMessage += "key = ";
            warningsMessage += warning.key;
            warningsMessage += " | parameter = ";
            warningsMessage += warning.parameter;
            if (warning.hasOwnProperty("value")) {
                warningsMessage += " | value = ";
                warningsMessage += warning.value;
            }
        }
        warningsMessage += "]";
        Scrutari.log(warningsMessage);
    }
};
Scrutari.Ajax.check = function (obj, name, defaultValue) {
    if (!obj.hasOwnProperty(name)) {
        obj[name] = defaultValue;
    }
};
Scrutari.Meta = function (engineInfo) {
    this.engineInfo = engineInfo;
};
Scrutari.Meta.load = function(callback, scrutariConfig) {
    var buffer = new Object();
    var _ajaxEnd = function() {
        var scrutariMeta = new Scrutari.Meta(buffer.engineInfo);
        callback(scrutariMeta);
    };
    var _engineInfoCallback = function (engineInfo) {
        buffer.engineInfo = engineInfo;
        _ajaxEnd();
    };
    Scrutari.Ajax.loadEngineInfo(_engineInfoCallback, scrutariConfig);
};
Scrutari.Meta.prototype.getEngineInfo = function () {
    return this.engineInfo;
};
Scrutari.Meta.prototype.getGlobalFicheCount = function () {
    return this.engineInfo.stats.fiche;
};
Scrutari.Meta.prototype.getGlobalLangFicheCount = function (langArray) {
    var ficheCount = 0;
    var length = this.engineInfo.stats.langArray.length;
    for(var i = 0; i < length; i++) {
        var langObj = this.engineInfo.stats.langArray[i];
        if ($.inArray(langObj.lang, langArray) !== -1) {
            ficheCount += langObj.fiche;
        }
    }
    return ficheCount;
};
Scrutari.Meta.prototype.getCorpusFicheCount = function (code) {
    var corpus = this.getCorpus(code);
    if (!corpus) {
        return 0;
    }
    return corpus.stats.fiche;
};
Scrutari.Meta.prototype.getCorpusLangFicheCount = function (code, langArray) {
    var corpus = this.getCorpus(code);
    if (!corpus) {
        return 0;
    }
    var ficheCount = 0;
    var length = corpus.stats.langArray.length;
    for(var i = 0; i < length; i++) {
        var langObj = corpus.stats.langArray[i];
        if ($.inArray(langObj.lang, langArray) !== -1) {
            ficheCount += langObj.fiche;
        }
    }
    return ficheCount;
};
Scrutari.Meta.prototype.getBaseArray = function (sortFunction) {
    var array = new Array();
    var baseMap = this.engineInfo.baseMap;
    var p=0;
    for(var prop in baseMap) {
        array[p] = baseMap[prop];
        p++;
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};
Scrutari.Meta.prototype.getLangArray = function (sortFunction) {
    var array = new Array();
    var length = this.engineInfo.stats.langArray.length;
    for(var i = 0; i < length; i++) {
        array[i] = this.engineInfo.stats.langArray[i];
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};
Scrutari.Meta.prototype.getCategoryArray = function (sortFunction) {
    var array = new Array();
    if (!this.engineInfo.hasOwnProperty("categoryMap")) {
        return array;
    }
    var categoryMap = this.engineInfo.categoryMap;
    var p=0;
    for(var prop in categoryMap) {
        array[p] = categoryMap[prop];
        p++;
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};
Scrutari.Meta.prototype.getCorpusArray = function (sortFunction) {
    var array = new Array();
    var corpusMap = this.engineInfo.corpusMap;
    var p=0;
    for(var prop in corpusMap) {
        array[p] = corpusMap[prop];
        p++;
    }
    if (sortFunction) {
        array = array.sort(sortFunction);
    }
    return array;
};
Scrutari.Meta.prototype.getAttributeArray = function (type) {
    if (!this.engineInfo.hasOwnProperty("attributes")) {
        return new Array();
    }
    if (!this.engineInfo.attributes.hasOwnProperty(type)) {
        return new Array();
    }
    return this.engineInfo.attributes[type];
};
Scrutari.Meta.prototype.getBase = function (code) {
    var key = "code_" + code;
    if (this.engineInfo.baseMap.hasOwnProperty(key)) {
        return this.engineInfo.baseMap[key];
    } else {
        return null;
    }
};
Scrutari.Meta.prototype.getCorpus = function (code) {
    var key = "code_" + code;
    if (this.engineInfo.corpusMap.hasOwnProperty(key)) {
        return this.engineInfo.corpusMap[key];
    } else {
        return null;
    }
};
Scrutari.Meta.prototype.getThesaurus = function (code) {
    var key = "code_" + code;
    if (this.engineInfo.thesaurusMap.hasOwnProperty(key)) {
        return this.engineInfo.thesaurusMap[key];
    } else {
        return null;
    }
};
Scrutari.Meta.prototype.getLangLabel = function (iso) {
    if (this.engineInfo.langMap.hasOwnProperty(iso)) {
        return this.engineInfo.langMap[iso];
    } else {
        return iso;
    }
};
Scrutari.Meta.prototype.withCategory = function () {
    return this.engineInfo.hasOwnProperty("categoryMap");
};
Scrutari.Meta.prototype.getCorpusArrayForCategories = function (categoryArray) {
    var result = new Array();
    if (!this.engineInfo.hasOwnProperty("categoryMap")) {
        return result;
    }
    for(var i = 0; i < categoryArray.length; i++) {
        var categoryName = categoryArray[i];
        if (this.engineInfo.categoryMap.hasOwnProperty(categoryName)) {
            result = result.concat(this.engineInfo.categoryMap[categoryName].codecorpusArray);
        }
    }
    return result;
};
Scrutari.Meta.prototype.getCorpusArrayForBases = function (baseArray) {
    var result = new Array();
    for(var i = 0; i < baseArray.length; i++) {
        var key = "code_" + baseArray[i];
        if (this.engineInfo.baseMap.hasOwnProperty(key)) {
            result = result.concat(this.engineInfo.baseMap[key].codecorpusArray);
        }
    }
    return result;
};
Scrutari.Meta.prototype.getComplementIntitule = function(code, complementNumber) {
    var corpus = this.getCorpus(code);
    if (!corpus) {
        return "";
    }
    var key = "complement_" + complementNumber;
    if (corpus.intitules.hasOwnProperty(key)) {
        return corpus.intitules[key];
    } else {
        return key;
    }
};
Scrutari.Utils = {};
Scrutari.Utils.divideIntoColumns = function (objectArray, jqArgument, htmlFunction) {
    var objectCount = objectArray.length;
    if (objectCount === 0) {
        return;
    }
    var $elements = Scrutari.convert(jqArgument);
    var elementCount = $elements.length;
    if (elementCount === 0) {
        Scrutari.log("HtmlElement selection with jqArgument is empty ");
        return;
    }
    var objectCount = objectArray.length;
    if (objectCount <= elementCount) {
        for(var i = 0; i < objectCount; i++) {
            $($elements[i]).append(htmlFunction(objectArray[i]));
        }
        return;
    }
    var modulo = objectCount % elementCount;
    var columnLength = (objectCount - modulo) / elementCount;
    var start = 0;
    var stop = 0;
    for(var i = 0; i< elementCount; i++) {
        var $element = $($elements[i]);
        stop += columnLength;
        if (i < modulo) {
            stop++;
        }
        for(var j = start; j < stop; j++) {
            $element.append(htmlFunction(objectArray[j]));
        }
        start = stop;
    }
};
Scrutari.Utils.checkPagination = function (ficheCount, plageLength, currentPlageNumber, jqArgument, htmlFunction) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.empty();
    var plageCount;
    if (ficheCount <= plageLength) {
        plageCount = 1;
        return;
    } else {
        var modulo = ficheCount % plageLength;
        plageCount = (ficheCount - modulo) / plageLength;
        if (modulo > 0) {
            plageCount ++;
        }
    }
    if (currentPlageNumber > plageCount) {
        currentPlageNumber = plageCount;
    }
    var plageNumberStart = 1;
    var plageNumberEnd = 9;
    if (currentPlageNumber > 6) {
        plageNumberStart = currentPlageNumber - 3;
        plageNumberEnd = currentPlageNumber + 3;
    }
    if (plageNumberEnd > plageCount) {
        plageNumberEnd = plageCount;
    }
    if (plageNumberStart > 1) {
        $elements.append(htmlFunction(1,"1",'enabled'));
        $elements.append(htmlFunction(0,"…",'disabled'));
    }
    for(var i = plageNumberStart; i <= plageNumberEnd; i++) {
        var state = 'enabled';
        if (i == currentPlageNumber) {
            state = 'active';
        }
        var html = htmlFunction(i, i, state);
        $elements.append(html);
    }
    if (plageNumberEnd < plageCount) {
        $elements.append(htmlFunction(0,"…",'disabled'));
    }
};
Scrutari.Utils.disable = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('disabled', true);
    return $elements;
};
Scrutari.Utils.enable = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('disabled', false);
    return $elements;
};
Scrutari.Utils.uncheck = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('checked', false);
    return $elements;
};
Scrutari.Utils.check = function (jqArgument) {
    var $elements = Scrutari.convert(jqArgument);
    $elements.prop('checked', true);
    return $elements;
};
Scrutari.Utils.toggle = function (jqElement, stateDataKey) {
    var state = jqElement.data(stateDataKey);
    if (state === 'off') {
        state = 'on';
    } else {
        state = 'off';
    }
    jqElement.data(stateDataKey, state);
    return state;
};
Scrutari.Utils.toggle.getState = function (jqArgument, stateDataKey) {
    var $elements = Scrutari.convert(jqArgument);
    return $elements.data(stateDataKey);
};
Scrutari.Utils.toggle.disabled = function (jqArgument, state) {
    var $elements = Scrutari.convert(jqArgument);
    if (state === 'off') {
        $elements.prop('disabled', true);
    } else {
        $elements.prop('disabled', false);
    }
    return $elements;
};
Scrutari.Utils.toggle.text = function (jqArgument, alterDataKey) {
    var $elements = Scrutari.convert(jqArgument);
    var length = $elements.length;
    for(var i = 0; i < length; i++) {
        var jqEl = $($elements[i]);
        var currentText =jqEl.text();
        var alterText = jqEl.data(alterDataKey);
        jqEl.text(alterText);
        jqEl.data(alterDataKey, currentText);
    }
    return $elements;
};
Scrutari.Utils.toggle.classes = function (jqArgument, state, onClass, offClass) {
    var $elements = Scrutari.convert(jqArgument);
    if (state === 'off') {
        $elements.addClass(offClass).removeClass(onClass);
    } else {
        $elements.removeClass(offClass).addClass(onClass);
    }
    return $elements;
};
Scrutari.Utils.sortBaseByFicheCountDesc = function (base1, base2) {
    var count1 = base1.stats.fiche;
    var count2 = base2.stats.fiche;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = base1.codebase;
        var code2 = base2.codebase;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};
Scrutari.Utils.sortLangByFicheCountDesc = function (lang1, lang2) {
    var count1 = lang1.fiche;
    var count2 = lang2.fiche;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = lang1.lang;
        var code2 = lang2.lang;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};
Scrutari.Utils.sortCategoryByRankDesc = function (category1, category2) {
    var count1 = category1.rank;
    var count2 = category2.rank;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = category1.name;
        var code2 = category2.name;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};
Scrutari.Utils.sortCorpusByFicheCountDesc = function (corpus1, corpus2) {
    var count1 = corpus1.stats.fiche;
    var count2 = corpus2.stats.fiche;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var code1 = corpus1.codecorpus;
        var code2 = corpus2.codecorpus;
        if (code1 < code2) {
            return -1;
        } else if (code1 > code2) {
            return 1;
        } else {
            return 0;
        }
    }
};
Scrutari.Utils.sortGroupByFicheCount = function (group1, group2) {
    var count1 = group1.ficheCount;
    var count2 = group2.ficheCount;
    if (count1 > count2) {
        return -1;
    } else if (count1 < count2) {
        return 1;
    } else {
        var rank1 = group1.category.rank;
        var rank2 = group1.category.rank;
        if (rank1 < rank2) {
            return -1;
        } else if (rank1 > rank2) {
            return 1;
        } else {
            return 0;
        }
    }
};
Scrutari.Utils.convert = function (jqArgument) {
    if (jqArgument.jquery) {
        return jqArgument;
    } else {
        return $(jqArgument);
    }
};
 Scrutari.Utils.escape = function (text) {
     return $('<div />').text(text).html();
 };
Scrutari.Filter = function () {
    this.withBase = false;
    this.withCategory = false;
    this.withBase = false;
    this.withCorpus = false;
    this.baseArray = new Array();
    this.langArray = new Array();
    this.categoryArray = new Array();
    this.corpusArray = new Array();
};
Scrutari.Filter.prototype.clearArrays = function () {
    while(this.baseArray.length > 0) {
        this.baseArray.pop();
    }
    while(this.langArray.length > 0) {
        this.langArray.pop();
    }
    while(this.categoryArray.length > 0) {
        this.categoryArray.pop();
    }
    while(this.corpusArray.length > 0) {
        this.corpusArray.pop();
    }
};
Scrutari.Filter.prototype.setWithBase = function (bool) {
    this.withBase = bool;
};
Scrutari.Filter.prototype.checkWithBase = function (jqArgument, stateDataKey, onValue) {
    var $elements = Scrutari.convert(jqArgument);
    var bool = ($elements.data(stateDataKey) === onValue);
    this.withBase = bool;
    return bool;
};
Scrutari.Filter.prototype.setWithLang = function (bool) {
    this.withLang = bool;
};
Scrutari.Filter.prototype.checkWithLang = function (jqArgument, stateDataKey, onValue) {
    var $elements = Scrutari.convert(jqArgument);
    var bool = ($elements.data(stateDataKey) === onValue);
    this.withLang = bool;
    return bool;
};
Scrutari.Filter.prototype.setWithCategory = function (bool) {
    this.withCategory = bool;
};
Scrutari.Filter.prototype.checkWithCategory = function (jqArgument, stateDataKey, onValue) {
    var $elements = Scrutari.convert(jqArgument);
    var bool = ($elements.data(stateDataKey) === onValue);
    this.withCategory = bool;
    return bool;
};
Scrutari.Filter.prototype.setWithCorpus = function (bool) {
    this.withCorpus = bool;
};
Scrutari.Filter.prototype.checkWithCorpus = function (jqArgument, stateDataKey, onValue) {
    var $elements = Scrutari.convert(jqArgument);
    var bool = ($elements.data(stateDataKey) === onValue);
    this.withCorpus = bool;
    return bool;
};
Scrutari.Filter.prototype.addBase = function (base) {
    this.baseArray.push(base);
};
Scrutari.Filter.prototype.addLang = function (lang) {
    this.langArray.push(lang);
};
Scrutari.Filter.prototype.addCategory = function (category) {
    this.categoryArray.push(category);
};
Scrutari.Filter.prototype.addCorpus = function (corpus) {
    this.corpusArray.push(corpus);
};
Scrutari.Filter.prototype.checkRequestParameters = function (scrutariMeta, requestParameters) {
    if (this.hasLang()) {
        requestParameters.langlist = this.langArray.join(",");
    }
    if (this.hasBase()) {
        requestParameters.baselist = this.baseArray.join(",");
    }
    if (this.hasCategory()) {
        requestParameters.categorylist = this.categoryArray.join(",");
    }
    if (this.hasCorpus()) {
        requestParameters.corpuslist = this.corpusArray.join(",");
    }
};
Scrutari.Filter.prototype.getFilterFicheCount = function (scrutariMeta) {
    if (!this.hasFilter()) {
        return scrutariMeta.getGlobalFicheCount();
    }
    var filterFicheCount = 0;
    if (this.hasCorpusFilter()) {
        var corpusMap = this.checkCorpusMap(scrutariMeta);
        var completeValue = corpusMap.completeValue;
        for(var key in corpusMap) {
            if (key === 'completeValue') {
                continue;
            }
            if (corpusMap[key] === completeValue) {
                var codecorpus = parseInt(key.substring(5));
                if (this.hasLang()) {
                    filterFicheCount += scrutariMeta.getCorpusLangFicheCount(codecorpus, this.langArray);
                } else {
                    filterFicheCount += scrutariMeta.getCorpusFicheCount(codecorpus);
                }
            }
        }
    } else {
        filterFicheCount += scrutariMeta.getGlobalLangFicheCount(this.langArray);
    }
    return filterFicheCount;
};
Scrutari.Filter.prototype.hasFilter = function () {
    if (this.hasLang()) {
        return true;
    }
    if (this.hasBase()) {
        return true;
    }
    if (this.hasCategory()) {
        return true;
    }
    if (this.hasCorpus()) {
        return true;
    }
    return false;
};
Scrutari.Filter.prototype.hasCorpusFilter = function () {
    if (this.hasBase()) {
        return true;
    }
    if (this.hasCategory()) {
        return true;
    }
    if (this.hasCorpus()) {
        return true;
    }
    return false;
};
Scrutari.Filter.prototype.checkCorpusMap = function (scrutariMeta) {
    var corpusMap = new Object();
    var finalCount = 0;
    if (this.hasCategory())  {
        finalCount++;
        var arrayForCategories = scrutariMeta.getCorpusArrayForCategories(this.categoryArray);
        for(var i = 0; i < arrayForCategories.length; i++) {
            var key = "code_" + arrayForCategories[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    if (this.hasBase())  {
        finalCount++;
        var arrayForBases = scrutariMeta.getCorpusArrayForBases(this.baseArray);
        for(var i = 0; i < arrayForBases.length; i++) {
            var key = "code_" + arrayForBases[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    if (this.hasCorpus())  {
        finalCount++;
        for(var i = 0; i < this.corpusArray.length; i++) {
            var key = "code_" + this.corpusArray[i];
            if (corpusMap.hasOwnProperty(key)) {
                corpusMap[key] = corpusMap[key] + 1;
            } else {
                corpusMap[key] = 1;
            }
        }
    }
    corpusMap.completeValue = finalCount;
    return corpusMap;
};
Scrutari.Filter.prototype.hasLang = function () {
    return ((this.withLang) && (this.langArray.length > 0));
};
Scrutari.Filter.prototype.hasBase = function () {
    return ((this.withBase) && (this.baseArray.length > 0));
};
Scrutari.Filter.prototype.hasCategory = function () {
    return ((this.withCategory) && (this.categoryArray.length > 0));
};
Scrutari.Filter.prototype.hasCorpus = function () {
    return ((this.withCorpus) && (this.corpusArray.length > 0));
};
Scrutari.Result = function (ficheSearchResult, groupSortFunction) {
    this.ficheSearchResult = ficheSearchResult;
    this.ficheGroupArray = ficheSearchResult.ficheGroupArray;
    if ((groupSortFunction) && (this.ficheGroupArray.length > 1)) {
        this.ficheGroupArray = this.ficheGroupArray.sort(groupSortFunction);
    }
    this.motcleMap = new Object();
    if (ficheSearchResult.hasOwnProperty("motcleArray")) {
        var length = ficheSearchResult.motcleArray.length;
        for(var i = 0; i < length; i++) {
            var motcle = ficheSearchResult.motcleArray[i];
            this.motcleMap["code_" + motcle.codemotcle] = motcle;
        }
    }
};
Scrutari.Result.newSearch = function (callback, scrutariConfig, requestParameters, scrutariErrorCallback) {
    var _ficheSearchResultCallback = function (ficheSearchResult) {
        callback(new Scrutari.Result(ficheSearchResult, scrutariConfig.getGroupSortFunction()));
    };
    Scrutari.Ajax.loadFicheSearchResult(_ficheSearchResultCallback, scrutariConfig, requestParameters, scrutariErrorCallback);
};
Scrutari.Result.prototype.getQId = function () {
    return this.ficheSearchResult.qId;
};
Scrutari.Result.prototype.getFormatedSearchSequence = function (scrutariLoc) {
    var q = this.ficheSearchResult.q;
    q = q.replace(/\&\&/g, scrutariLoc.loc('_ scrutari-and'));
    q = q.replace(/\|\|/g, scrutariLoc.loc('_ scrutari-or'));
    return q;
};
Scrutari.Result.prototype.getFicheCount = function () {
    return this.ficheSearchResult.ficheCount;
};
Scrutari.Result.prototype.getFicheGroupType = function () {
    return this.ficheSearchResult.ficheGroupType;
};
Scrutari.Result.prototype.getNoneFicheArray = function () {
    if (this.ficheGroupArray.length === 0) {
        return new Array();
    }
    return this.ficheGroupArray[0].ficheArray;
};
Scrutari.Result.prototype.selectNoneFicheArray = function (plageLength, plageNumber) {
    var selectionArray = new Array();
    if (this.ficheGroupArray.length === 0) {
        return selectionArray;
    }
    var ficheArray = this.ficheGroupArray[0].ficheArray;
    var startIndex = plageLength * (plageNumber - 1);
    var length = ficheArray.length;
    if (startIndex >= length) {
        return selectionArray;
    }
    var min = Math.min(ficheArray.length, startIndex + plageLength);
    for(var i = startIndex; i < min; i++) {
        selectionArray.push(ficheArray[i]);
    }
    return selectionArray;
};
Scrutari.Result.prototype.isNonePlageLoaded = function (plageLength, plageNumber) {
    if (this.ficheGroupArray.length === 0) {
        return true;
    }
    var ficheCount = this.ficheSearchResult.ficheCount;
    var ficheArray = this.ficheGroupArray[0].ficheArray;
    var length = ficheArray.length;
    if (length === ficheCount) {
        return true;
    }
    var endIndex = (plageLength * plageNumber) - 1;
    if (endIndex < length) {
        return true;
    }
    return false;
};
Scrutari.Result.prototype.loadNonePlage = function (callback, scrutariConfig, plageLength, plageNumber) {
    if (this.ficheGroupArray.length === 0) {
        return true;
    }
    var group = this.ficheGroupArray[0];
    if (!group) {
        return;
    }
    var ficheCount = this.ficheSearchResult.ficheCount;
    var ficheArray = group.ficheArray;
    var length = ficheArray.length;
    if (length === ficheCount) {
        return;
    }
    var _existingFicheSearchResultCallback = function (ficheSearchResult) {
        var newCount = ficheSearchResult.ficheGroupArray.length;
        if (newCount > 0) {
            group.ficheArray = group.ficheArray.concat(ficheSearchResult.ficheGroupArray[0].ficheArray);
        }
        callback();
    };
    var requestParameters = {
        qid: this.ficheSearchResult.qId,
        start: length +1,
        limit: (plageLength * (plageNumber + 2)) - length
    };
    Scrutari.Ajax.loadExistingFicheSearchResult(_existingFicheSearchResultCallback, scrutariConfig, requestParameters); 
};
Scrutari.Result.prototype.isCategoryPlageLoaded = function (categoryName, plageLength, plageNumber) {
    var group = this.getFicheGroupByCategoryName(categoryName);
    if (!group) {
        return true;
    }
    var categoryFicheCount = group.ficheCount;
    var ficheArray = group.ficheArray;
    var length = ficheArray.length;
    if (length === categoryFicheCount) {
        return true;
    }
    var endIndex = (plageLength * plageNumber) - 1;
    if (endIndex < length) {
        return true;
    }
    return false;
};
Scrutari.Result.prototype.loadCategoryPlage = function (callback, scrutariConfig, categoryName, plageLength, plageNumber) {
    var group = this.getFicheGroupByCategoryName(categoryName);
    if (!group) {
        return;
    }
    var categoryFicheCount = group.ficheCount;
    var ficheArray = group.ficheArray;
    var length = ficheArray.length;
    if (length === categoryFicheCount) {
        return;
    }
    var _existingFicheSearchResultCallback = function (ficheSearchResult) {
        var newCount = ficheSearchResult.ficheGroupArray.length;
        for(var i = 0; i < newCount; i++) {
            var newGroup = ficheSearchResult.ficheGroupArray[i];
            if (newGroup.category.name === group.category.name) {
                group.ficheArray = group.ficheArray.concat(newGroup.ficheArray);
            }
        }
        callback();
    };
    var requestParameters = {
        qid: this.ficheSearchResult.qId,
        start: length +1,
        limit: (plageLength * (plageNumber + 2)) - length,
        starttype: "in:" + categoryName
    };
    Scrutari.Ajax.loadExistingFicheSearchResult(_existingFicheSearchResultCallback, scrutariConfig, requestParameters); 
};
Scrutari.Result.prototype.selectCategoryFicheArray = function (categoryName, plageLength, plageNumber) {
    var selectionArray = new Array();
    var ficheArray = this.getCategoryFicheArrayByName(categoryName);
    var startIndex = plageLength * (plageNumber - 1);
    var length = ficheArray.length;
    if (startIndex >= length) {
        return selectionArray;
    }
    var min = Math.min(ficheArray.length, startIndex + plageLength);
    for(var i = startIndex; i < min; i++) {
        selectionArray.push(ficheArray[i]);
    }
    return selectionArray;
};
Scrutari.Result.prototype.getCategoryCount = function () {
    return this.ficheGroupArray.length;
};
Scrutari.Result.prototype.getCategory = function (index) {
    return this.ficheGroupArray[index].category;
};
Scrutari.Result.prototype.getFicheGroupByCategoryName = function (categoryName) {
    var groupCount = this.ficheGroupArray.length;
    for(var i = 0; i < groupCount; i++) {
        var group = this.ficheGroupArray[i];
        if ((group.category) && (group.category.name === categoryName)) {
            return group;
        }
    }
    return null;
};
Scrutari.Result.prototype.getCategoryFicheCount = function (index) {
    return this.ficheGroupArray[index].ficheCount;
};
Scrutari.Result.prototype.getCategoryFicheCountbyName = function (categoryName) {
    var groupCount = this.ficheGroupArray.length;
    for(var i = 0; i < groupCount; i++) {
        var group = this.ficheGroupArray[i];
        if ((group.category) && (group.category.name === categoryName)) {
            return group.ficheCount;
        }
    }
    return 0;
};
Scrutari.Result.prototype.getCategoryFicheArray = function (index) {
    return this.ficheGroupArray[index].ficheArray;
};
Scrutari.Result.prototype.getCategoryFicheArrayByName = function (categoryName) {
    var categoryCount = this.getCategoryCount();
    for(var i = 0; i < categoryCount; i++) {
        var category = this.getCategory(i);
        if (category.name === categoryName) {
            return this.getCategoryFicheArray(i);
        }
    }
    return new Array();
};
Scrutari.Result.prototype.getMotcle = function (code) {
    var key = "code_" + code;
    if (this.motcleMap.hasOwnProperty(key)) {
        return this.motcleMap[key];
    } else {
        return null;
    }
};
Scrutari.Html = {};
Scrutari.Html.base = function (base) {
    var code = base.codebase;
    var html = "";
    html += "<p>";
    html += "<label><input type='checkbox' name='base' value='" + code + "'> ";
    if (base.baseicon) {
        html += "<img src='" + base.baseicon + "' alt=''> ";
    }
    html += Scrutari.escape(base.intitules.short);
    html += " (" + base.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};
Scrutari.Html.initLangHtmlFunction = function (scrutariMeta) {
    var _langHtmlFunction = function (lang) {
        var code = lang.lang;
        var html = "";
        html += "<p>";
        html += "<label><input type='checkbox' name='lang' value='" + code + "'> ";
        var label = scrutariMeta.getLangLabel(code);
        if (label !== code) {
            html += label;
            html += " ";
        }
        html += "[" + code + "]";
        html += " (" + lang.fiche +")";
        html += "</label>";
        html += "</p>";
        return html;
    };
    return _langHtmlFunction;
};
Scrutari.Html.category = function (category) {
    var name = category.name;
    var html = "";
    html += "<p>";
    html += "<label><input type='checkbox' name='category' value='" + name + "'> ";
    html +=  Scrutari.escape(category.title);
    html += " (" + category.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};
Scrutari.Html.corpus = function (corpus) {
    var code = corpus.codecorpus;
    var html = "";
    html += "<p>";
    html += "<label><input type='checkbox' name='corpus' value='" + code + "'> ";
    html += Scrutari.escape(corpus.intitules.corpus);
    html += " (" + corpus.stats.fiche +")";
    html += "</label>";
    html += "</p>";
    return html;
};
Scrutari.Html.initFicheArrayHtmlFunction = function (scrutariMeta, scrutariLoc, scrutariResult) {
    var ficheHtmlFunction =  Scrutari.Html.initFicheHtmlFunction(scrutariMeta, scrutariLoc, scrutariResult);
    var _ficheArrayHtmlFunction = function (ficheArray) {
        var html = "";
        var length = ficheArray.length;
        for(var i = 0; i < length; i++) {
            html += ficheHtmlFunction(ficheArray[i]);
        }
        return html;
    }
    return _ficheArrayHtmlFunction;
 };
 Scrutari.Html.initFicheHtmlFunction = function (scrutariMeta, scrutariLoc, scrutariResult, target) {
     var _attrToString = function (mattrMap, attributeArray) {
         var html = "";
         var length = attributeArray.length;
         for(var i = 0; i < length; i++) {
            var attribute = attributeArray[i];
            if (mattrMap.hasOwnProperty(attribute.name)) {
                 var valueArray = mattrMap[attribute.name];
                 var valLength = valueArray.length;
                 html += "<p class='scrutari-fiche-Attribute'>";
                 html += '<span class="scrutari-intitule-Attribute">';
                 html += attribute.title;
                 html += scrutariLoc.loc("_ scrutari-colon");
                 html += '</span>';
                 if (attribute.type === "block") {
                     html += '</p><p class="scrutari-fiche-AttrBlocks">…<br/>';
                     for(var j = 0; j< valLength; j++) {
                        if (j > 0) {
                            html += "<br/>…<br/>";
                        }
                        html += Scrutari.Html.mark(valueArray[j]);
                     }
                     html += '<br/>…</p>';
                 } else {
                     html += ' ';
                     for(var j = 0; j< valLength; j++) {
                        if (j > 0) {
                            html += ", ";
                        }
                        html += Scrutari.Html.mark(valueArray[j]);
                     }
                     html += "</p>";
                 }
            }
         }
         return html;
     };
     var _ficheHtmlFunction = function (fiche) {
        var html = "";
        html += "<div class='scrutari-fiche-Block' id='ficheBlock_" + fiche.codefiche + "'>";
        if (fiche.hasOwnProperty("icon")) {
            html += "<div class='scrutari-fiche-Icon'><img alt='' src='" + fiche.icon + "'/></div>";
        }
        html += "<p class='scrutari-fiche-Titre'>";
        html += "<a href='" + fiche.href + "'";
        html += " class='scrutari-fiche-Link'";
        html += " id='ficheLink_" + fiche.codefiche + "'";
        if (target.length > 0) {
            html += " target='" + target + "'";
        }
        html += ">";
        if (fiche.hasOwnProperty("mtitre")) {
            html += Scrutari.Html.mark(fiche.mtitre);
        } else {
            html += fiche.href;
        }
        html += "</a>";
        html += "</p>";
        if (fiche.hasOwnProperty("msoustitre")) {
            html += "<p class='scrutari-fiche-Soustitre'>";
            html += Scrutari.Html.mark(fiche.msoustitre);
            html += "</p>";
        }
        if (fiche.hasOwnProperty("annee")) {
            html += "<p class='scrutari-fiche-Annee'>";
            html += fiche.annee;
            html += "</p>";
        }
        if (fiche.hasOwnProperty("mattrMap")) {
            html += _attrToString(fiche.mattrMap, scrutariMeta.getAttributeArray("primary"));
        }
        if (fiche.hasOwnProperty("mcomplementArray")) {
            var complementLength = fiche.mcomplementArray.length;
            for(var i = 0; i < complementLength; i++) {
                var complement = fiche.mcomplementArray[i];
                html += "<p class='scrutari-fiche-Complement'>";
                html += '<span class="scrutari-intitule-Complement">';
                html += scrutariMeta.getComplementIntitule(fiche.codecorpus, complement.num);
                html += scrutariLoc.loc("_ scrutari-colon");
                html += '</span> ';
                html += Scrutari.Html.mark(complement.mcomp);
                html += "</p>";
            }
        }
        if (fiche.hasOwnProperty("mattrMap")) {
            html += _attrToString(fiche.mattrMap, scrutariMeta.getAttributeArray("secondary"));
        }
        if (fiche.hasOwnProperty('codemotcleArray')) {
            var length = fiche.codemotcleArray.length;
            html += '<p class="scrutari-fiche-Motcle">';
            html += '<span class="scrutari-intitule-Motcle">';
            if (length === 1) {
                html += scrutariLoc.loc("_ scrutari-motscles_un");
            } else {
                html += scrutariLoc.loc("_ scrutari-motscles_plusieurs");
            }
            html += '</span> ';
            for(var i = 0; i < length; i++) {
                if (i > 0) {
                    html += ", ";
                }
                var code = fiche.codemotcleArray[i];
                var motcle = scrutariResult.getMotcle(code);
                var libelleLength = motcle.mlibelleArray.length;
                for(var j = 0; j < libelleLength; j++) {
                    if (j > 0) {
                        html += "/";
                    }
                    html += Scrutari.Html.mark(motcle.mlibelleArray[j].mlib);
                }
            }
            html += "</p>";
        }
        html += '</div>';
        return html;
    };
    return _ficheHtmlFunction;
 };
 Scrutari.Html.mark = function (markArray) {
    var html = "";
    var length = markArray.length;
    for (var i = 0; i < length; i++) {
        var obj = markArray[i];
        if (typeof obj === 'string') {
            html += obj;
        } else if (obj.s) {
            html += "<span class='scrutari-Mark'>";
            html += Scrutari.escape(obj.s);
            html += "</span>";
        }
    }
    return html;
};
Scrutari.Html.initPaginationHtmlFunction = function (paginationIdPrefix) {
    var _paginationHtmlFunction = function (plageNumber, title, state) {
        var html = "";
        if (state === 'active') {
            html += "<li class='active'>";
            html += "<span>";
            html += title;
            html += "</span>";
            html += "</li>";
        } else if (state === 'disabled') {
            html += "<li class='disabled'>";
            html += "<span>";
            html += title;
            html += "</span>";
            html += "</li>";
        } else {
            html += "<li>";
            html += "<a href='#" + paginationIdPrefix + plageNumber + "'>";
            html += title;
            html += "</a>";
            html += "</li>";
        }
        return html;   
    };
    return _paginationHtmlFunction;
};
Scrutari.Html.updateFilter = function (scrutariFilter) {
    scrutariFilter.clearArrays();
    var $baseElements = $("input[name='base']:checked");
    for(var i = 0; i < $baseElements.length; i++) {
        scrutariFilter.addBase(parseInt($baseElements[i].value));
    }
    var $langElements = $("input[name='lang']:checked");
    for(var i = 0; i < $langElements.length; i++) {
        scrutariFilter.addLang($langElements[i].value);
    }
    var $categoryElements = $("input[name='category']:checked");
    for(var i = 0; i < $categoryElements.length; i++) {
        scrutariFilter.addCategory($categoryElements[i].value);
    }
    var $corpusElements = $("input[name='corpus']:checked");
    for(var i = 0; i < $corpusElements.length; i++) {
        scrutariFilter.addCorpus(parseInt($corpusElements[i].value));
    }
};