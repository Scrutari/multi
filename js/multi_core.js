/***************************************************************
* Multi - Exemole d'appel de différents moteurs Scrutari
* 
* Copyright (c) 2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

/**
 * @namespace
 */
var Multi = {};

Multi.lang = "fr";
Multi.url = "http://sct1.scrutari.net/sct/_json";
Multi.group = "ritimo";
Multi.scrutariConfigArray = new Array();

Multi.init = function () {
    var $resultArea = $("#resultArea");
    $("#mainsearchForm").submit(function () {
        var q = $(this).find("input[name='q']").val();
        q = $.trim(q);
        if (q.length > 0) {
            var configLength = Multi.scrutariConfigArray.length;
            for(var i = 0; i < configLength; i++) {
                var scrutariConfig = Multi.scrutariConfigArray[i];
                Multi.loadSearch(scrutariConfig, q);
            }
        }
        return false;
    });
    Multi.loadEngineGroup(Multi.engineGroupCallback);
};

Multi.loadSearch = function (scrutariConfig, q) {
    var name = scrutariConfig.getName();
    var requestParameters = {};
    requestParameters["log"] = "all";
    requestParameters["q"] = q;
    var _resultCallback = function (scrutariResult) {
        var $resultText = $("#resultText_" + name);
        var ficheCount = scrutariResult.getFicheCount();
        if (ficheCount == 0) {
            $resultText.html("<em>" + "Aucun résultat" + "</em>");
        } else {
            var url = "http://scrutarijs.coredem.info/?engine=" + scrutariConfig.getName()
                + "&hide=mainform,history,subsearch"
                + "&qid=" +  scrutariResult.getQId();
            var html = "<a target='Result' href='" + url + "'>";
            if (ficheCount == 1) {
                html += "Un résultat";
            } else {
                html += ficheCount + " résultats";
            }
            html += "</a>";
            $resultText.html(html);
        }
    };
    var _errorCallback = function (error) {

    };
    Scrutari.Result.newSearch(_resultCallback, scrutariConfig, requestParameters, _errorCallback);
};

Multi.engineGroupCallback = function (engineGroup) {
    $("#groupTitle").html(engineGroup.title);
    var $resultArea = $("#resultArea");
    var engineLength = engineGroup.engineArray.length;
    for(var i = 0; i < engineLength; i++) {
        var engine = engineGroup.engineArray[i];
        var $div = $("<div/>").attr("id", "resultArea_" + engine.name);
        if (engine.icon) {
            $div.append($("<p/>").attr("class", "multi-engine-Icon").append($("<img/>").attr("src", engine.icon)));
        }
        var $engineTitle = $("<p/>").attr("class", "multi-engine-Title").html(engine.title);
        if (engine.website) {
            $engineTitle.append(" ").append($("<span/>").attr("class", "multi-engine-Website").append("(").append($("<a/>").attr("target", "_blank").attr("href", engine.website).html("site")).append(")"));
        }
        $div.append($engineTitle);
        if (engine.description) {
            $div.append($("<p/>").attr("class", "multi-engine-Description").html(engine.description));
        }
        $div.append($("<p/>").attr("class", "multi-engine-Result").text(">> ").append($("<span/>").attr("id", "resultText_" + engine.name)));
        $resultArea.append($div);
        var scrutariConfig = new Scrutari.Config(engine.name, engine.url, Multi.lang, "js-multi");
        scrutariConfig.setPlageLength(5);
        Multi.scrutariConfigArray.push(scrutariConfig);
    }
};

Multi.loadEngineGroup = function (engineGroupCallback, requestParameters) {
    if (!requestParameters) {
        requestParameters = new Object();
    }
    requestParameters.type = "enginegroup";
    requestParameters.name = Multi.group;
    Scrutari.Ajax.check(requestParameters, "lang", Multi.lang);
    Scrutari.Ajax.check(requestParameters, "warnings", 1);
    Scrutari.Ajax.check(requestParameters, "version", 1);
    $.ajax({
        url: Multi.url,
        dataType: "jsonp",
        data: requestParameters,
        success: function (data, textStatus) {
            Scrutari.Ajax.success(data, "engineGroup", engineGroupCallback);
        }
    });
};