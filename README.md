Cette application est un exemple d'interrogation simultanée de plusieurs moteurs Scrutari.

Pour faire fonctionner le projet, il suffit d'installer les fichiers dans n'importe quel répertoire d'un serveur avec PHP. L'appel se fait via index.php auquel il faut rajouter le paramètre group qui indique le nom du groupe de moteur du serveur sct1.scrutari.net. Voir la page http://www.scrutari.net/dokuwiki/serveurscrutari:config:group pour plus d'information sur la configuration d'un groupe de moteurs.

Exemples :

Sites clés en main de Ritimo : http://apps.coredem.info/multi/index.php?group=siclem_ritimo

Coredem et médias libres : http://apps.coredem.info/multi/index.php?group=coredem_medias