<?php
/***************************************************************
* Multi - Exemole d'appel de différents moteurs Scrutari
* 
* Copyright (c) 2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

$lang = "fr";
if (isset($_REQUEST['lang'])) {
    switch($_REQUEST['lang']) {
        case 'fr':
            $lang = $_REQUEST['lang'];
    }
}

$group = "";
if (isset($_REQUEST['group'])) {
    $groupParam = $_REQUEST['group'];
    if (preg_match('/^[-a-zA-Z_]+$/', $groupParam)) {
        $group = $groupParam;
    } else {
        exit("Malformed group");
    }
}
if (strlen($group) == 0) {
    exit("Undefined group");
}


?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
<title>MultiScrutari</title>
<script src="jquery/1.11.2/jquery.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="bootstrap/3.3.7/css/bootstrap.min.css">
<script src="bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/multi.css" rel="stylesheet" type="text/css" />

<script src="js/scrutari.js"></script>
<script src="js/multi_core.js"></script>
<script>
Multi.lang = "<?php echo $lang; ?>";
Multi.url = "http://sct1.scrutari.net/sct/_json";
Multi.group = "<?php echo $group; ?>"
$(function () {
  Multi.init();  
});
</script>
</head>
<body>
<div id="bodyArea">
    <p id="groupTitle" class="multi-MainTitle"></p>
    <form action="#" id="mainsearchForm">
    <input name="q" type="text" size="15" id="qInput">
    <input type="submit" value="Rechercher" id="mainSubmit">
    </form>
    <p class="multi-ListTitle">Liste des moteurs :</p>
    <div id="resultArea">
    </div>
    <p class="multi-Mentions">Code du projet déposé sur <a href="https://framagit.org/Scrutari/multi" target="_blank">FramaGit</a> (sous licence MIT)</p>
</div>
</body>
</html>