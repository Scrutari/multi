<?php
/***************************************************************
*
* Copyright (c) 2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

$lang = "fr";
if (isset($_REQUEST['lang'])) {
    switch($_REQUEST['lang']) {
        case 'fr':
            $lang = $_REQUEST['lang'];
    }
}

$width=300;

$group = "";
if (isset($_REQUEST['group'])) {
    $groupParam = $_REQUEST['group'];
    if (preg_match('/^[-a-zA-Z_]+$/', $groupParam)) {
        $group = $groupParam;
    } else {
        exit("Malformed group");
    }
}
if (strlen($group) == 0) {
    exit("Undefined group");
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Recherche Scrutari multiple</title>
    <link href="images/icon.png" type="image/png" rel="icon">
</head>
<frameset cols="<?php echo $width; ?>,*" >
    <frame name="Search" src="search.php?group=<?php echo  $group; ?>">
    <frame name="Result" src="blank.php">
</frameset>
</html>