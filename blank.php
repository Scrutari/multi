<?php
/***************************************************************
*
* Copyright (c) 2017 Vincent Calame - Exemole
* Licensed under MIT (http://en.wikipedia.org/wiki/MIT_License)
*************************************************************/

$lang = "fr";
if (isset($_REQUEST['lang'])) {
    switch($_REQUEST['lang']) {
        case 'fr':
            $lang = $_REQUEST['lang'];
    }
}
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
<title>Blanc</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
</html>